import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;

public class MainClass {
    public static void main(String[] args) {
        int LENGTH = 8;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter start point:(0 up to 60 except 1,13,19,32,37,41,45,47,50,53 which are black homes)");
        int startPoint = scanner.nextInt();
        System.out.println("Enter last point:");
        int lastPoint = scanner.nextInt();

        int[][] graph = new int[LENGTH * LENGTH][5];
        //set graph of bombs and free space
        prepareGraph(graph);



        //array for saving parent
        int[] vertixParent = new int[LENGTH * LENGTH];

        boolean pathFind = bfsSerach(graph, LENGTH, vertixParent, startPoint, lastPoint);
        if (pathFind) {
            char[] ch = new char[LENGTH * LENGTH];
            int chCounter = 0;
            int lastIndex = vertixParent[lastPoint];
            int last = lastPoint;
            while (lastPoint!=startPoint) {
                if (lastIndex + 8 == lastPoint) {
                    ch[chCounter] = 'D';
                    chCounter++;
                } else if (lastIndex - 8 == lastPoint) {
                    ch[chCounter] = 'U';
                    chCounter++;
                } else if (lastIndex - 1 == lastPoint) {
                    ch[chCounter] = 'L';
                    chCounter++;
                } else if (lastIndex + 1 == lastPoint) {
                    ch[chCounter] = 'R';
                    chCounter++;
                }
                lastPoint = lastIndex;
                lastIndex = vertixParent[lastIndex];

            }

            ch = Arrays.copyOf(ch,chCounter);
            for (int i = ch.length - 1; i >= 0; i--) {
                System.out.print(ch[i]+" ");
            }


        } else {
            System.out.println("There is sth wrong with inputs");
        }


    }

    public static void prepareGraph(int arr[][]) {
        arr[1][0] = 1;
        arr[13][0] = 1;
        arr[19][0] = 1;
        arr[32][0] = 1;
        arr[37][0] = 1;
        arr[41][0] = 1;
        arr[45][0] = 1;
        arr[47][0] = 1;
        arr[50][0] = 1;
        arr[53][0] = 1;



        int leftCounter = 0;
        int rightCounter = 7;
        for (int i = 0; i < 64; i++) {
            if (i < 8) {
                if (i == 0) {
                    arr[i][1] = i + 1;
                    arr[i][2] = -1;
                    arr[i][3] = -1;
                    arr[i][4] = i + 8;
                } else if (i == 7) {
                    arr[i][1] = -1;
                    arr[i][2] = i - 1;
                    arr[i][3] = -1;
                    arr[i][4] = i + 8;
                } else {
                    arr[i][1] = i + 1;
                    arr[i][2] = i - 1;
                    arr[i][3] = -1;
                    arr[i][4] = i + 8;
                }
            } else if (i >= 57) {
                if (i == 57) {
                    arr[i][1] = i + 1;
                    arr[i][2] = -1;
                    arr[i][3] = i - 8;
                    arr[i][4] = -1;
                } else if (i == 63) {
                    arr[i][1] = -1;
                    arr[i][2] = i - 1;
                    arr[i][3] = i - 8;
                    arr[i][4] = -1;
                } else {
                    arr[i][1] = i + 1;
                    arr[i][2] = i - 1;
                    arr[i][3] = i - 8;
                    arr[i][4] = -1;
                }
            } else if (leftCounter + 8 == i) {
                arr[i][1] = i + 1;
                arr[i][2] = -1;
                arr[i][3] = i - 8;
                arr[i][4] = i + 8;
                leftCounter += 8;
            } else if (rightCounter + 8 == i) {
                arr[i][1] = -1;
                arr[i][2] = i - 1;
                arr[i][3] = i - 8;
                arr[i][4] = i + 8;
                rightCounter += 8;
            } else {
                arr[i][1] = i + 1;
                arr[i][2] = i - 1;
                arr[i][3] = i - 8;
                arr[i][4] = i + 8;
            }


        }
    }

    public static boolean bfsSerach(int[][] arr, int arrLength, int[] visitedIndexArr, int startPoint, int endPoint) {
        boolean[] visited = new boolean[arrLength * arrLength];

        //setting all of items of arraies to non-checkable mode
        for (int i = 0; i < arrLength * arrLength; i++) {
            visited[i] = false;
            visitedIndexArr[i] = -1;

        }
        //init array for saving appropriate vertexs
        int[] stack = new int[arrLength * arrLength];
        int stackCounter = 0;
        stack[stackCounter] = startPoint;
        stackCounter++;
        //setting init step
        visited[startPoint] = true;
        int parentCounter = 0;
        //bfs algorithm
        while (parentCounter != stackCounter) {
            int parent = stack[parentCounter];
            //for loop for checing four side of a vertix
            for (int i = 1; i < 5; i++) {
                //checking sides if exists
                if (arr[parent][i] != -1) {
                    //checking bombs
                    if (arr[arr[parent][i]][0] != 1) {
                        if (visited[arr[parent][i]] == false) {
                            visited[arr[parent][i]] = true;
                            visitedIndexArr[arr[parent][i]] = parent;
                            stack[stackCounter] = arr[parent][i];
                            stackCounter++;

                            if (arr[parent][i] == endPoint) {
                                return true;
                            }
                        }
                    }
                }
            }
            parentCounter++;

        }
        return false;
    }
}
